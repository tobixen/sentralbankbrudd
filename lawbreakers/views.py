# -*- coding: utf-8 -*-
# Copyright (c) 2018-2019 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

from django.contrib.gis.geos import Polygon
from django.shortcuts import redirect, render

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .forms import PrivacyEraserForm
from .models import PrivacyEraser
from .serializers import  PrivacyEraserSerializer, PrivacyEraserGeoJSONSerializer

def frontpage(request):
    return render(request, 'frontpage.html', {
    })


def new_lawbreaker(request):
    if request.method == 'POST':
        form = PrivacyEraserForm(request.POST)
        if form.is_valid():
            # FIXME add reporter information
            eraser = form.save()
            return redirect('lawbreakers-frontpage')
    else:
        form = PrivacyEraserForm()
    return render(request,
                  'new_lawbreaker.html',
                  {'form': form})


@api_view(['GET'])
def api_root(request, format=None):
    """The root of the API / web services.  Provide links to the provided
    API endpoints.

    """

    return Response({
        '_links' : {
            'lawbreaker': reverse('lawbreakers-api-lawbreaker', request=request),
            'geojson':     reverse('lawbreakers-geojson', request=request),

        },
    })


class ApiLawbreakers(generics.ListAPIView):
    """Return list of registered lawbreakers in JSON format.

    """

    queryset = PrivacyEraser.objects.all()
    serializer_class = PrivacyEraserSerializer


class ApiGeoJSON(generics.ListAPIView):
    """Return list of registered lawbreakers in GeoJSON format.

    """

    queryset = PrivacyEraser.objects.all()
    serializer_class = PrivacyEraserGeoJSONSerializer
