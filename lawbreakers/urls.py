# -*- coding: utf-8 -*-
# Copyright (c) 2018 Petter Reinholdtsen <pere@hungry.com>
# This file is covered by the GPLv2 or later, read COPYING for details.

from django.conf.urls import url
from django.contrib import admin

from .views import *

urlpatterns = [
    url(r'^api$', api_root, name='lawbreakers-api-root'),
    url(r'^api/lawbreaker', ApiLawbreakers.as_view(), name='lawbreakers-api-lawbreaker'),

    url(r'^geojson', ApiGeoJSON.as_view(), name='lawbreakers-geojson'),
    url(r'^entries/add', new_lawbreaker, name='lawbreakers-new-lawbreaker'),
    url(r'^', frontpage, name='lawbreakers-frontpage'),
]
